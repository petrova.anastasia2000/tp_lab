﻿
#include <iostream>
#include "Header.h"

const std::string SKIP_CHARACTERS = " ";

void ClearInputStream(std::istream& in)
{
	in.clear();
	while (in.peek() != '\n' && in.peek() != EOF)
	{
		in.get();
	}
}

int Seek(std::istream& in)
{
	while (in.peek() != '\n' && SKIP_CHARACTERS.find((char)in.peek()) != std::string::npos)
	{
		in.get();
	}
	return in.peek();
}

bool CheckBounds(int n)
{
	bool ok = (0 <= n && n <= +1000000000);
	return ok;
}

int ReadInt(std::istream& in)
{
	int ans;
	in >> ans;
	while (!in || Seek(in) != '\n' || !CheckBounds(ans))
	{
		ClearInputStream(in);
		std::cout << "Input error, try again > ";
		in >> ans;
	}
	return ans;
}

bool is_continue() {
	std::string line;
	ClearInputStream(std::cin);
	std::getline(std::cin, line);
	while (true) {
		std::cout << "Continue? (Y/N) > ";
		std::getline(std::cin, line);
		if (line == "y" || line == "Y") return true;
		if (line == "n" || line == "N") return false;
	}
}

int main()
{
	do
	{
		std::cout << "\nEnter number > ";
		int num = ReadInt(std::cin);
		int sys;
		do {
			std::cout << "Enter sys > ";
			sys = ReadInt(std::cin);
		} while (sys < 2);

		std::cout << "Result > " << toStringSys(num, sys) << std::endl;
	} while (is_continue());
    return 0;
}