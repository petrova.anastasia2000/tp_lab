#pragma once

#ifdef TPLIBDLL_EXPORTS
#define TPLIBDLL_API __declspec(dllexport)
#else
#define TPLIBDLL_API __declspec(dllimport)
#endif

#include <string>

extern "C" TPLIBDLL_API std::string toStringSys(int number, int sys = 10);