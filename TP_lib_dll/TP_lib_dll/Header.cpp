#include "pch.h"
#include "Header.h"
#include <sstream>
#include <vector>

const char* laters = "0123456789ABCDEF";

std::string toStringSys(int number, int sys)
{
    std::vector<std::string> word;
    std::stringstream str;
    do {
        int t = number % sys;
        number /= sys;
        if (sys <= 16) word.push_back(std::string() + laters[t]);
        else {
            word.push_back(std::to_string(t));
            word.push_back("-");
        }
    } while (number);
    if(sys > 16) word.pop_back();
    for (auto x = word.rbegin(); x != word.rend(); x++)
        str << *x;
    return str.str();
}
